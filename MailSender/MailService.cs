﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMailer.Interfaces;
using Model;
using NLog;


namespace MailSender
{
    public class MailService : IMailService
    {
            private static Logger logger = LogManager.GetCurrentClassLogger();

            private readonly IFluentMailer _fluentMailer;
            private readonly IAddicionalMailService _savelastsendedmail;

            public MailService(IFluentMailer fluentMailer, IAddicionalMailService savelastsendedmaid)
            {
                _fluentMailer = fluentMailer;
                _savelastsendedmail = savelastsendedmaid;
            }
      
            public void SendMessage(int id,List<Email> model)
            {

            //_savelastsendedmail.SaveLastSendedMailId(0);
            var list = _savelastsendedmail.CounterMail(id, model);
            var last = model.Last();

            foreach (var item in list)
            {
                try
                {
        
                        var message = _fluentMailer.CreateMessage();
                        var mailSender = message.WithView("~MailTemplate.html", item)
                            .WithReceiver(item.EmailAddress)
                            .WithSubject(item.Subject);
                        mailSender.Send();
                        _savelastsendedmail.SaveLastSendedMailId(item.Id);                     
                        logger.Info("Wiadmosc wysłana do :" + item.EmailAddress + " Id maila : " + item.Id);
                        if (item == last)
                        {
                            logger.Info("Wszystkie maile wysłane");
                            _savelastsendedmail.SaveLastSendedMailId(0);
                        //stop
                    }



                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }

            }

        }    
        
    }
}
