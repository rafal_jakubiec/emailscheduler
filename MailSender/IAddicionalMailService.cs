﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailSender
{
    public interface IAddicionalMailService
    {
        void SaveLastSendedMailId(int id);
        List<Email> CounterMail(int id, List<Email> EmailList);
    }
}
