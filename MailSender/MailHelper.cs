﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSVMapper;
using FluentMailer.Factory;
using System.Configuration;

namespace MailSender
{
    public class MailHelper
    {

        public void MailFactory()
        {
            IMailService _imailservice;
            ICSVMapperService _icsvreader;
            IAddicionalMailService _IAddicionalMailService;
            _icsvreader = new CSVMapperService();
            _IAddicionalMailService = new AddicionalMailService();
            var fluentMailer = FluentMailerFactory.Create();
            _imailservice = new MailService(fluentMailer, _IAddicionalMailService);
            var SendedMailId = Convert.ToInt32(ConfigurationManager.AppSettings["LastSendedEmailId"]);

            _imailservice.SendMessage(SendedMailId, _icsvreader.CSVMap());

        }
    }
}
