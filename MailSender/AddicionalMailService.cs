﻿using Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailSender
{
    public class AddicionalMailService : IAddicionalMailService
    {
        public void SaveLastSendedMailId(int id)
        {
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            var entry = config.AppSettings.Settings["LastSendedEmailId"];
            if (entry == null)
                config.AppSettings.Settings.Add("LastSendedEmailId", id.ToString());
            else
                config.AppSettings.Settings["LastSendedEmailId"].Value = id.ToString();

            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection(config.AppSettings.SectionInformation.Name);
        }

        public List<Email> CounterMail(int MailsToSend, List<Email> EmailList)
        {
            if (EmailList.Count() > MailsToSend + 10)
            {
               return EmailList.GetRange(MailsToSend, 10);
            }
            else
            {
                return EmailList.GetRange(MailsToSend, EmailList.Count()- MailsToSend);
            }
        }

    }
}
