﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quartz;
using NLog;
using Quartz.Impl;
using System.Threading;
using Common.Logging.NLog;


namespace Schedulers
{
   public class Scheduler
    {
          private static Logger logger = LogManager.GetCurrentClassLogger();

        public Scheduler()
        {
            try
            {
               // Common.Logging.LogManager.Adapter = new Common.Logging.Simple.ConsoleOutLoggerFactoryAdapter { Level = Common.Logging.LogLevel.Info };

                IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();

                scheduler.Start();

                IJobDetail job = JobBuilder.Create<SendingMailJob>()
                    .WithIdentity("SendingMail", "task")
                    .Build();

                ITrigger trigger = TriggerBuilder.Create()
                    .WithIdentity("TriggerSendingMail", "task")
                    .StartNow()                  
                    .WithSimpleSchedule(x => x
                        .WithIntervalInSeconds(60)
                        .RepeatForever())
                    .Build();

                scheduler.ScheduleJob(job, trigger);
                logger.Info("Scheduler pracuje");
                //scheduler.Shutdown();
            }
            catch (SchedulerException se)
            {
                logger.Error(se);
            }
        }

    }
           
    
}
