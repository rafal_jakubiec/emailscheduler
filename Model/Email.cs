﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Email
    {
        public int Id { get; set; }
        public string EmailAddress { get; set; }
        public string Subject { get; set; }
        public string Reciever { get; set; }

    }
}
