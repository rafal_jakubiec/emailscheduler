﻿using Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CSVMapper
{
    public class CSVMapperService : ICSVMapperService
    {
        public List<Email> CSVMap()
        {
          
            string[] allLines = File.ReadAllLines(Path.Combine(Environment.CurrentDirectory, "emailList.csv"));

            var query = from line in allLines
                        let data = line.Split(',')
                        select new Email()
                        {
                            Id = Convert.ToInt32(data[0]),
                            EmailAddress = data[1],
                            Subject = data[2],
                            Reciever = data[3]
                        };

            return query.ToList();
        }
      
    }
}
