﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSVMapper
{
    public interface ICSVMapperService
    {
        List<Email> CSVMap();
    }
}
